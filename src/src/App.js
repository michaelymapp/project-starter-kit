import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import BottomNavigationBar from './components/BottomNavigationBar';
import config from './app-config';
import Login from './components/Login';
import Home from './components/Home';
import Me from './components/Account';
import EditProfile from './components/EditProfile';

class TestComponent extends Component {
	render() {
		return (
			<div>
				<p>This was from a test component</p>
			</div>
		)
	}
}

class App extends Component {
	render() {
    return (
			<MuiThemeProvider muiTheme={config.muiTheme}>
				<Switch>
					<Route exact path='/' component={Home} />
					<Route path='/chats' component={TestComponent} />
					<Route path='/forums' component={Home} />
					<Route path='/me/editprofile' component={EditProfile} />
					<Route path='/me' component={Me} />
				</Switch>
				<BottomNavigationBar />
			</MuiThemeProvider>
		);
  }
}

export default App;
