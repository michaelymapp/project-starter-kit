const socket = require('socket.io');

module.exports = (app) => {
	let io = socket(app);
	io.on('connection', function(socket) {
		console.log('a user connected');
	});
}
