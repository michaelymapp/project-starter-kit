import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import {Card, CardHeader, CardTitle, CardText} from 'material-ui/Card';
import config from '../../app-config';
import API from '../../api';

class ThreadListCard extends Component {

	constructor(props) {
		super(props);
		this.browseToThread = this.browseToThread.bind(this);
	}

	browseToThread() {
		// TODO: Browse to this forum by it's id
		// this.props.history.push(`/forums/thread/${this.state.threadid}/view`);
		// the next page will list the posts in the thread
	}

	render() {
		return (
      <Card onClick={this.browseToThread}>
        <CardHeader title={this.props.threadtitle} />
				<CardText>
					{this.props.threaddescription}
				</CardText>
      </Card>
		);
  }
}

export default withRouter(ThreadListCard);
