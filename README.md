# Project Starter Kit

This will be the base for all community applications from here-on-in, it uses:

- material-ui
- react
- react-dom
- redux
- ajv
- styled-components
- express
- body-parser
- rethinkdb
- webpack
- typescript
- passport
- passport-jwt
- now

## API

The API is REST based, determining by version numbers. Legacy APIs must be kept based on version number to prevent application breakage.
