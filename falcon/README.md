# Falcon Server

The Falcon Server is a lightweight server designed to run on ```now``` for the backend of all applications you produce.

## Stack

- Passport
- Passport-jwt
- Express
- Mongoose
- rethinkdb (optional)

## Optional

You could implement Next.js to provide server-side rendering, but this is not included in the package.
