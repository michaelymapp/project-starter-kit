module.exports = {
	API_VERSION: '1',
	API_PREFIX: 'v',
	API_ROUTE_PREFIX: `/api/${this.API_PREFIX}${this.API_VERSION}/`,
	CONSTANTS: {
		USE_JWT: true,
		USE_MONGODB: true,
		PRODUCTION_MODE: process.env.NODE_ENV === 'PRODUCTION' ? true : false
	},
	DATABASE: {
		PRODUCTION: {
			host: 'prod-aws.mongodb-cluster.on.mongodb.cloud',
			user: 'app-name1234',
			pass: 'app-pass1234'
		},
		DEVELOPMENT: {}
	}
};
