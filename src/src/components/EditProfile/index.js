import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import {Card, CardHeader, CardTitle, CardText} from 'material-ui/Card';
import AppBar from 'material-ui/AppBar';
import config from '../../app-config';
import API from '../../api';
//import API from '../../api';
import Drawer from 'material-ui/Drawer';
import MenuItem from 'material-ui/MenuItem';
import Badge from 'material-ui/Badge';
import IconButton from 'material-ui/IconButton';
import Login from '../Login';

class EditProfile extends Component {

	constructor(props) {
		super(props);
		this.handleToggleDrawer = this.handleToggleDrawer.bind(this);
		this.clearSessionData = this.clearSessionData.bind(this);
		this.getSupport = this.getSupport.bind(this);
		this.submitForm = this.submitForm.bind(this);
		var me = JSON.parse(sessionStorage.getItem('login_account'));
		this.state = {
			username: me.username,
			password: '',
			displayname: me.displayname,
			email: me.email,
			open: false
		};
	}

	submitForm() {
		/** TODO: Post to backend */
		/**
		 * API.post(`/user/me/savechanges`, this.state)
		 *    .then((res) => {
		 *			var data = res.data;
		 *			if(data.status === 'OK') {
		 *				this.props.history.push('/me');
		 *			}
		 *			else {
		 *				alert(data.message);
		 *			}
		 *		})
		 *		.catch(() => {
		 *			alert('Something went wrong, please try again');
		 *		});
		 */
		var person = JSON.parse(sessionStorage.getItem('login_account'));
		var state  = this.state;
		var statekeys = Object.keys(state);
		statekeys.forEach((key) => {
			person[key] = state[key];
		});
		person = JSON.stringify(person);
		sessionStorage.setItem('login_account', person);
		this.props.history.push('/me');
	}

	getSupport() {}

	clearSessionData() {
		sessionStorage.clear();
		this.setState({ username: '' });
	}
	
	handleToggleDrawer() {
		this.setState({ open: true });
	}

	render() {
		if(sessionStorage.getItem('login_account') === null) {
			return (
				<Login />
			);
		}
		return (
			<div>
				<AppBar title="Edit Profile" onLeftIconButtonClick={this.handleToggleDrawer} />
				<Drawer docked={false} width={200} open={this.state.open} onRequestChange={(open) => this.setState({ open })}>
					<MenuItem onClick={this.getSupport}>
						Need help?
					</MenuItem>
					<MenuItem onClick={this.clearSessionData}>
						Sign Out
					</MenuItem>
				</Drawer>
				<Card>
					<CardText>
						<form onSubmit={this.submitForm}>
						<TextField hintText="Name to display in forums/chat" floatingLabelText="Display Name" defaultValue={this.state.displayname} onChange={(event,newValue) => this.setState({displayname:newValue})} />
						<br />
						<TextField hintText="Enter your desired username" floatingLabelText="Desired Username" defaultValue={this.state.username} onChange={(event,newValue) => this.setState({username:newValue})} />
            <br />
            <TextField type="password" hintText="Enter your new password" floatingLabelText="New Password" onChange={(event,newValue) => this.setState({password: newValue})} />
            <br />
						<TextField type="email" hintText="Enter your email address" floatingLabelText="Email Address" defaultValue={this.state.email}  onChange={(event,newValue) => this.setState({email: newValue})} />
						<br />
            <RaisedButton secondary={true} label="Save Settings" onClick={this.submitForm} />
						</form>
					</CardText>
				</Card>
			</div>
		);
  }
}

export default withRouter(EditProfile);
