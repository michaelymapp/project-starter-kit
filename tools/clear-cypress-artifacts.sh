#!/bin/bash
set -e

#############################################################################
#	This script will delete all artifact assets in the following directories: #
#		$CURRENT_WORKING_DIRECTORY:																							#
#			-> cypress/screenshots/**																							#
#			-> cypress/videos/**																									#
#			-> cypress/artifact-partials/**																				#
#############################################################################

cwd=`pwd`
CWD_CYPRESS_SCREENSHOTS="${cwd}/cypress/screenshots/*"
CWD_CYPRESS_VIDEOS="${cwd}/cypress/videos/*"
CWD_CYPRESS_ARTIFACT_PARTIALS="${cwd}/cypress/artifact-partials/*"

rm -rf $CWD_CYPRESS_SCREENSHOTS
rm -rf $CWD_CYPRESS_VIDEOS
rm -rf $CWD_CYPRESS_ARTIFACT_PARTIALS

echo "Deleted the following files in the directories listed:"
echo $CWD_CYPRESS_SCREENSHOTS
echo $CWD_CYPRESS_VIDEOS
echo $CWD_CYPRESS_ARTIFACT_PARTIALS

exit 0
