import React, { Component } from 'react';
import {BottomNavigation, BottomNavigationItem} from 'material-ui/BottomNavigation';
import { withRouter } from 'react-router-dom';
import Paper from 'material-ui/Paper';
import CommunicationChat from 'material-ui/svg-icons/communication/chat';
import CommunicationForum from 'material-ui/svg-icons/communication/forum';
import ActionAccountCircle from 'material-ui/svg-icons/action/account-circle';

const groupsIcon = <CommunicationChat />;
const forumIcon = <CommunicationForum />;
const accountIcon = <ActionAccountCircle />;

class BottomNavigationBar extends Component {
	state = {
		selectedIndex: 2,
		toPage: null
	}

	getToPage(num) {
		if(num === 0) {
			return '/chats';
		} else if(num === 1) {
			return '/forums';
		} else if(num === 2) {
			return '/me';
		}
	}

	select = num => {
		this.props.history.push(this.getToPage(num) || '/');
		this.setState({ selectedIndex: num, toPage: this.getToPage(num) })
	}

	render() {
		return (
				<Paper zDepth={1} style={{position: "fixed", bottom: "0", width:"100%"}}>
					<BottomNavigation selectedIndex={this.state.selectedIndex}>
						<BottomNavigationItem onClick={() => this.select(0)} label="Chats" icon={groupsIcon} />
						<BottomNavigationItem onClick={() => this.select(1)} label="Forum" icon={forumIcon} />
						<BottomNavigationItem onClick={() => this.select(2)} label="Account" icon={accountIcon} />
					</BottomNavigation>
				</Paper>
		);
  }
}

export default withRouter(BottomNavigationBar);
