import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import {Card, CardHeader, CardTitle, CardText} from 'material-ui/Card';
import config from '../../app-config';
import API from '../../api';

class ForumListCard extends Component {

	constructor(props) {
		super(props);
		this.state = {
			forumid: props.forumid,
			forum_name: '',
			forum_description: '',
			forum_icon: ''
		};
		this.browseToForum = this.browseToForum.bind(this);
		this.getForumInformation();
	}

	getForumInformation() {
		API.post(`/forum/category/${this.state.forumid}/getinfo`, {})
		   .then((res) => {
				 var data = res.data;
				 this.setState({
					 forum_name: data.forum_name,
					 forum_description: data.forum_description,
					 forum_icon: data.forum_icon
				 });
			 })
			 .catch(() => {
				 this.setState({ forum_name: 'Error', forum_description: 'There was an error finding this forum', forum_icon: '' });
			 });
	}

	browseToForum() {
		// TODO: Browse to this forum by it's id
		// this.props.history.push(`/forums/category/${this.state.forumid}/view`);
		// the next page will list THREADS in the forum category
	}

	render() {
		return (
      <Card onClick={this.browseToForum}>
        <CardHeader title={this.state.forum_name} avatar={this.state.forum_icon} />
				<CardText>
					{this.state.forum_description}
				</CardText>
      </Card>
		);
  }
}

export default withRouter(ForumListCard);
