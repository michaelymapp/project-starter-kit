import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import {Card, CardHeader, CardTitle, CardText} from 'material-ui/Card';
import AppBar from 'material-ui/AppBar';
import config from '../../app-config';
//import API from '../../api';
import Drawer from 'material-ui/Drawer';
import MenuItem from 'material-ui/MenuItem';
import Badge from 'material-ui/Badge';
import IconButton from 'material-ui/IconButton';
import Login from '../Login';
import Chip from 'material-ui/Chip';
import {red500, green500} from 'material-ui/styles/colors';

class Me extends Component {

	constructor(props) {
		super(props);
		const style = {
			chip: {
				margin: 4,
				display: 'inline-block'
			}
		}
		this.handleToggleDrawer = this.handleToggleDrawer.bind(this);
		this.openEditProfilePage = this.openEditProfilePage.bind(this);
		this.clearSessionData = this.clearSessionData.bind(this);
		var me = JSON.parse(sessionStorage.getItem('login_account'));
		var chatinfo = JSON.parse(sessionStorage.getItem('chat_info'));
		var badges = JSON.parse(sessionStorage.getItem('badges'));
		var badgelist = [];
		if(badges !== null)
		{
			badges.forEach((badge) => {
				if(typeof badge.is_rare !== 'undefined' && badge.is_rare) {
					if(badge.title === 'Administrator') {
						// Site Admin Badge
						badgelist.push(
							<Chip key={badge.title.toString()} style={style.chip} backgroundColor={red500}>
								{badge.title}
							</Chip>
						)
					}
					else if(badge.title === 'Moderator') {
						// Site Moderator Badge
						badgelist.push(
							<Chip key={badge.title.toString()} style={style.chip} backgroundColor={green500}>
								{badge.title}
							</Chip>
						)
					}
				}
				else {
					badgelist.push(<Chip key={badge.title.toString()} style={style.chip}>{badge.title}</Chip>);
				}
			});
		}
		if(badgelist.length === 0) {
			badgelist.push(
				<Chip style={style.chip}>
					Welcome To {config.app_name}
				</Chip>
			);
		}
		if(me === null) {
			me = {
				username: 'guest',
				displayname: 'guest',
				email: 'guest@guest.com'
			}
			var chatinfo = {
				groupcount: 0,
				pmcount: 0
			};
		}
		this.state = {
			username: me.username || 'guest',
			displayname: me.displayname || 'guest',
			email: me.email || 'guest',
			conversation_count: chatinfo.groupcount || 0,
			pm_count: chatinfo.pmcount || 0,
			open: false,
			badgelist: badgelist,
		};
	}

	clearSessionData() {
		sessionStorage.clear();
		this.setState({ username: '' });
	}
	openEditProfilePage() {
		this.props.history.push('/me/editprofile');
	}

	handleToggleDrawer() {
		this.setState({ open: true });
	}

	render() {
		if(sessionStorage.getItem('login_account') === null) {
			return (
				<Login />
			);
		}
		return (
			<div>
				<AppBar title="My Account" onLeftIconButtonClick={this.handleToggleDrawer} />
				<Drawer docked={false} width={200} open={this.state.open} onRequestChange={(open) => this.setState({ open })}>
					<MenuItem onClick={this.openEditProfilePage}>
						Edit Profile
					</MenuItem>
					<MenuItem onClick={this.clearSessionData}>
						Sign Out
					</MenuItem>
				</Drawer>
				<Card>
					<CardText>
						<p>Hi there {this.state.displayname}! Thanks for using our application. You are off to a great start, you have had {this.state.conversation_count} group conversations and {this.state.pm_count} private conversations! :)</p>
						<hr />
						<h3>Badges</h3>
						{this.state.badgelist}
					</CardText>
				</Card>
			</div>
		);
  }
}

export default withRouter(Me);
