module.exports = app => {
	/**
	 *
	 * TODO: Autoload appliaction-related data inside of here, apply routes, etc
	 *
	 */
	app.get('/', (req, res) => res.send({ status: 'ONLINE' }));
	app.post('/api/v1/user/signin', (req, res) => require('./routes/user/signin')(req, res));
	app.post('/api/v1/users/me/getdetails', (req, res) => require('./routes/user/me')(req, res));
};
