import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import {Card, CardHeader, CardTitle, CardText} from 'material-ui/Card';
import AppBar from 'material-ui/AppBar';
import config from '../../app-config';
//import API from '../../api';
import Drawer from 'material-ui/Drawer';
import MenuItem from 'material-ui/MenuItem';
import IconButton from 'material-ui/IconButton';

const styles = {
	notification: {
		backgroundColor: 'green',
		color: 'white',
		borderRadius: '50%',
		width: '24px',
		height: '24px',
		fontSize: '12px',
		paddingLeft: 8,
		paddingRight: 8,
		paddingTop: 4,
		paddingBottom: 4,
		marginLeft: 10,
		fontWeight: 500
	}
};

class Home extends Component {

	constructor(props) {
		super(props);
		this.handleToggleDrawer = this.handleToggleDrawer.bind(this);
		this.openNotificationsDialog = this.openNotificationsDialog.bind(this);
		this.state = {
			username: '',
			open: false,
			notification_count: 1
		};
	}

	openNotificationsDialog() {}

	handleToggleDrawer() {
		this.setState({ open: true });
	}

	render() {
		if(sessionStorage.getItem('login_account') === null) {
			this.props.history.push('/me');
		}
		return (
			<div>
				<AppBar title="Home" onLeftIconButtonClick={this.handleToggleDrawer} />
				<Drawer docked={false} width={200} open={this.state.open} onRequestChange={(open) => this.setState({ open })}>
					<MenuItem onClick={this.openNotificationsDialog}>
						Notifications <span style={styles.notification}>{this.state.notification_count}</span>
					</MenuItem>
				</Drawer>
				<Card>
					<CardHeader title="Home" />
					<CardText>
						<p>Lorem ipsum here</p>
					</CardText>
				</Card>
			</div>
		);
  }
}

export default withRouter(Home);
