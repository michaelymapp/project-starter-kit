import {green100, green500, green700} from 'material-ui/styles/colors';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import axios from 'axios';
const muiTheme = getMuiTheme({
  palette: {
    primary1Color: green500,
    primary2Color: green700,
    primary3Color: green100
  }
},
  {
    avatar: {
      borderColor: null
    }
  });

const config = {
	app_name: 'React Application',
  muiTheme,
  signin: {
    title: 'Welcome, let\'s get started!'
  },
	api_server_domain: `http://localhost:2018`,
	api_prefix: `/api/v1`
};

export default config;
