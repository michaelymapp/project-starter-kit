import config from './app-config';
import axios from 'axios';

export default axios.create({
  baseURL: `${config.api_server_domain}${config.api_prefix}`
});
