import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import {Card, CardHeader, CardTitle, CardText} from 'material-ui/Card';
import config from '../../app-config';
import API from '../../api';

class Login extends Component {

	// select = num => {
	// 	// this.props.history.push(this.getToPage(num) || '/');
	// 	// this.setState({ selectedIndex: num, toPage: this.getToPage(num) })
	// }
  state = {
    username: '',
    password: '',
		error: false,
		error_message: ''
  }

  handleClick(event) {
		let username = this.state.username;
		let password = this.state.password;
		API.post(`/user/signin`, { username, password })
		     .then(res => {
				   console.log(res); // we expect if successful, a JWT token, and a login_unique_id token
					 if(typeof res.data.jwt !== 'undefined' && res.data.status === 'OK') {
						 sessionStorage.setItem('login_account', JSON.stringify(res.data));
						 sessionStorage.setItem('chat_info', '{"groupcount": 0, "pmcount": 0}');
						 sessionStorage.setItem('badges', '[]');
						 this.props.history.push('/me');
					 }
					 else {
						this.setState({ error: true, error_message: 'You have entered an invalid username or password combination. Please try again.' });
					 }
				 })
				 .catch(res => {
						this.setState({ error: true, error_message: 'There was an error attempting to sign-in, please try again' });
				 });
	}

	render() {
		return (
      <Card>
        <CardHeader title={config.signin.title} subtitle="" />
        <CardTitle title="Sign into your account" />
        <CardText>
					<p>{this.state.error_message}</p>
					<form onSubmit={this.handleClick}>
						<TextField hintText="Enter your username" floatingLabelText="Username" onChange = {(event,newValue) => this.setState({username:newValue})} />
						<br />
						<TextField type="password" hintText="Enter your password" floatingLabelText="Password" onChange={(event,newValue) => this.setState({password: newValue})} />
						<br />
						<RaisedButton label="Sign In" onClick={(event) => this.handleClick(event)} />
					</form>
        </CardText>
      </Card>
		);
  }
}

export default withRouter(Login);
